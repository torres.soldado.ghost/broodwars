package com.hexperiments.broodwars;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.hexperiments.broodwars.game.Assets;
import com.hexperiments.broodwars.game.screens.TestScreen0;
import com.hexperiments.broodwars.screens.game.transitions.DirectedGame;
import com.hexperiments.broodwars.utils.GamePreferences;

public class Broodwars extends DirectedGame {
	@SuppressWarnings("unused")
	private static final String TAG = Broodwars.class.getName();

	@Override
	public void create() {
		// set libgdx log level
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		// load assets
		
		Assets.instance.init(new AssetManager());
		
		// load preferences for audio settings and start playing music
		GamePreferences.instance.load();
		
		// start game at menu screen
		setScreen(new TestScreen0(this));
	}

}
