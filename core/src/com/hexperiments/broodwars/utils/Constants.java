package com.hexperiments.broodwars.utils;

public class Constants {
	// Visible game world is 800 meters wide
	public static final float VIEWPORT_WIDTH = 800.0f;
	
	// Visible game world is 480 meters tall
	public static final float VIEWPORT_HEIGHT = 480.0f;
	
	// GUI width
	public static final float VIEWPORT_GUI_WIDTH = 800.0f;
	
	// GUI height
	public static final float VIEWPORT_GUI_HEIGHT = 480.0f;
	
	// Location of texture altas file descriptors
	public static final String TEXTURE_ALTAS_OBJECTS = "images/game.pack";
	public static final String TEXTURE_ATLAS_LIBGDX_UI = "images/uiskin.atlas";
	
	// Location of description file for skin
	public static final String SKIN_LIBGDX_UI = "images/uiskin.json";
	
	// Game preferences file
	public static final String PREFERENCES = "game.prefs";
	
	// Location of image file for level 00
	public static final String LEVEL_00 = "levels/level0.png";
	public static final String LEVEL_01 = "levels/level1.png";
	
	// Debugging
	public static final boolean DEBUG_ENABLED = false;
}
