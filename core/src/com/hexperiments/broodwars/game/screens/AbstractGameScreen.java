package com.hexperiments.broodwars.game.screens;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.hexperiments.broodwars.game.Assets;
import com.hexperiments.broodwars.screens.game.transitions.DirectedGame;

public abstract class AbstractGameScreen implements Screen {
	protected DirectedGame game;
	public abstract InputProcessor getInputProcessor();
	
	public AbstractGameScreen(DirectedGame game) {
		this.game = game;
	}

	public abstract void render(float delta);
	public abstract void resize(int width, int height);
	public abstract void show();
	public abstract void hide();
	public abstract void pause();

	public void resume() {
		Assets.instance.init(new AssetManager());
	}

	public void dispose() {
		Assets.instance.dispose();
	}

}
