package com.hexperiments.broodwars.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.hexperiments.broodwars.game.WorldController;
import com.hexperiments.broodwars.game.WorldRenderer;
import com.hexperiments.broodwars.screens.game.transitions.DirectedGame;
import com.hexperiments.broodwars.utils.GamePreferences;

public class TestScreen0 extends AbstractGameScreen {
	public static final String TAG = TestScreen0.class.getName();

	private WorldController worldController;
	private WorldRenderer worldRenderer;
	private boolean paused;
	
	public TestScreen0(DirectedGame game) {
		super(game);
	}

	@Override
	public InputProcessor getInputProcessor() {
		Gdx.app.debug(TAG, "Setting input processor");
		return worldController;
	}

	@Override
	public void render(float deltaTime) {
		// Do not update the game when paused
		if (!paused) {
			worldController.update(deltaTime);
		}
		
		// Set screen clear color to black
		Gdx.gl.glClearColor(0, 0, 0, 1);
		// Clears the screen
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		// Render world to screen
		worldRenderer.render();
	}

	@Override
	public void resize(int width, int height) {
		worldRenderer.resize(width, height);
	}

	@Override
	public void show() {
		GamePreferences.instance.load();
		worldController = new WorldController(game);
		worldRenderer = new WorldRenderer(worldController);
		Gdx.input.setCatchBackKey(true);
	}

	@Override
	public void hide() {
		worldController.dispose();
		worldRenderer.dispose();
		Gdx.input.setCatchBackKey(false);
	}

	@Override
	public void pause() {
		paused = true;
	}
	
	@Override
	public void resume() {
		super.resume();
		paused = false;
	}

}
