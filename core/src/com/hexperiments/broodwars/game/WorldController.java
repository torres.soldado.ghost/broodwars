package com.hexperiments.broodwars.game;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;
import com.hexperiments.broodwars.screens.game.transitions.DirectedGame;
import com.hexperiments.broodwars.utils.CameraHelper;
import com.hexperiments.broodwars.utils.Constants;

public class WorldController extends InputAdapter implements Disposable {
	private static final String TAG = WorldController.class.getName();
	
	@SuppressWarnings("unused")
	private DirectedGame game;
	public Level level;	
	
	public CameraHelper cameraHelper; // Used to move and zoom the camera.
	
	/* We need this reference from the WorldRenderer 
	to translate screen coordinates into the world coordinates. */
	private OrthographicCamera worldCameraRef; 
	Vector3 worldCoordinates;
	
	/**
	 * Constructor
	 * @param game Compositor of rendered screens. Allows transitions between scenes using the RTT
	 * (Render To Texture) method.
	 */
	public WorldController(DirectedGame game) {
		this.game = game;
		init();
	}
	
	public void setWorldCamera(OrthographicCamera camera) {
		worldCameraRef = camera;
	}
	
	/**
	 * Initialize and center the cameraHelper.
	 * Load the game level.
	 */
	private void init() {
		cameraHelper = new CameraHelper();
		cameraHelper.setPosition(Constants.VIEWPORT_WIDTH / 2, Constants.VIEWPORT_HEIGHT / 2);
		worldCoordinates = new Vector3();

		level = new Level();
	}
	
	public void update(float deltaTime) {
		if (Constants.DEBUG_ENABLED) {
			handleDebugInput(deltaTime);
		}
		handleInputGame(deltaTime);
		level.update(deltaTime);
	}
	
	@Override
	public boolean keyUp(int keycode) {
		if (Constants.DEBUG_ENABLED) {
			if (keycode == Keys.R) { // Reset game world
				init();
				Gdx.app.debug(TAG, "User triggered world reset");
			}
		}
		return false;
	}
	
	public void handleInputGame(float deltaTime) {
		if (Gdx.input.isTouched()) {
			worldCoordinates.x = Gdx.input.getX();
			worldCoordinates.y = Gdx.input.getY();
			worldCameraRef.unproject(worldCoordinates);
			level.setTargetCoordinates((int) worldCoordinates.x, (int) worldCoordinates.y);
		}
	}

	@Override
	public void dispose() {
	}
	
	/**
	 * Only called if Constants.DEBUG_ENABLED is true.
	 * @param deltaTime
	 */
	private void handleDebugInput(float deltaTime) {
		if (Gdx.app.getType() != Application.ApplicationType.Desktop) return;
		
		// Camera controls (move)
		float cameraMoveSpeed = 50 * deltaTime;
		final float cameraMoveAccel = 5;
		if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT)) cameraMoveSpeed *= cameraMoveAccel;
		if (Gdx.input.isKeyPressed(Keys.LEFT)) moveCamera(-cameraMoveSpeed, 0);
		if (Gdx.input.isKeyPressed(Keys.RIGHT)) moveCamera(cameraMoveSpeed, 0);
		if (Gdx.input.isKeyPressed(Keys.UP)) moveCamera(0, cameraMoveSpeed);
		if (Gdx.input.isKeyPressed(Keys.DOWN)) moveCamera(0, -cameraMoveSpeed);
		if (Gdx.input.isKeyPressed(Keys.BACKSPACE)) cameraHelper.setPosition(0, 0);
		
		// Camera controls (zoom)
		float cameraZoomSpeed = 1 * deltaTime;
		float cameraZoomAccel = 5;
		if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT)) cameraZoomSpeed *= cameraZoomAccel;
		if (Gdx.input.isKeyPressed(Keys.COMMA)) cameraHelper.addZoom(cameraZoomSpeed);
		if (Gdx.input.isKeyPressed(Keys.PERIOD)) cameraHelper.addZoom(-cameraZoomSpeed);
		if (Gdx.input.isKeyPressed(Keys.SLASH)) cameraHelper.setZoom(1);
	}
	
	private void moveCamera(float x, float y) {
		x += cameraHelper.getPosition().x;
		y += cameraHelper.getPosition().y;
		cameraHelper.setPosition(x, y);
	}
}
