package com.hexperiments.broodwars.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Disposable;
import com.hexperiments.broodwars.utils.Constants;

public class Assets implements Disposable, AssetErrorListener {
	public static final String TAG = Assets.class.getName();
	public static final Assets instance = new Assets();
	private AssetManager assetManager;
	public AssetFonts fonts;
	public AssetMusic music;
	public AssetSounds sounds;
	public AssetAgent agent;
	public AssetLevels levels;
	public AssetSolid solidColors;
	
	// Singleton: prevent instantiation from other classes
	private Assets() {}
	
	public class AssetSounds {		
		public AssetSounds(AssetManager am) {
		}
	}
	
	public class AssetMusic {		
		public AssetMusic(AssetManager am) {
		}
	}
	
	public class AssetFonts {
		public final BitmapFont defaultSmall;
		public final BitmapFont defaultNormal;
		public final BitmapFont defaultBig;
		
		public AssetFonts() {
			// Create three fonts using libgdx's 15px font
			defaultSmall = new BitmapFont(Gdx.files.internal("images/arial-15.fnt"), true);
			defaultNormal = new BitmapFont(Gdx.files.internal("images/arial-15.fnt"), true);
			defaultBig = new BitmapFont(Gdx.files.internal("images/arial-15.fnt"), true);
			
			// Set font sizes
			defaultSmall.setScale(0.20f);
			defaultNormal.setScale(0.5f);
			defaultBig.setScale(2.0f);
			
			// Enable linear filtering
			defaultSmall.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
			defaultNormal.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
			defaultBig.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		}
	}
	
	public class AssetAgent {
		public final AtlasRegion agent;
		public AssetAgent(TextureAtlas atlas) {
			agent = atlas.findRegion("drone");
		}
	}
	
	public class AssetSolid {
		public final AtlasRegion red;
		public final AtlasRegion black;
		public AssetSolid(TextureAtlas atlas) {
			red = atlas.findRegion("red");
			red.getTexture().setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
			black = atlas.findRegion("black");
			black.getTexture().setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
		}
	}
	
	public class AssetLevels {
		public final AtlasRegion level00;
		public final AtlasRegion level01;
		public AssetLevels(TextureAtlas atlas) {
			level00 = atlas.findRegion("level0Bgnd");
			level00.getTexture().setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
			level01 = atlas.findRegion("level1Bgnd");
			level01.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		}
	}
	
	public void init(AssetManager assetManager) {
		this.assetManager = assetManager;
		
		// Set asset manager error listener
		assetManager.setErrorListener(this);
		
		// Load texture atlas
		assetManager.load(Constants.TEXTURE_ALTAS_OBJECTS, TextureAtlas.class);
		
		// load sounds

		// load music
		
		// Start loading assets and wait until finished
		assetManager.finishLoading();
		
		Gdx.app.log(TAG, "Loading assets:");
		for (String a: assetManager.getAssetNames()) {
			Gdx.app.log(TAG, a + " loaded.");
		}
		Gdx.app.log(TAG, "#" + assetManager.getAssetNames().size + " assets loaded.");
		
		TextureAtlas atlas = assetManager.get(Constants.TEXTURE_ALTAS_OBJECTS);
		
		// Enable texture filtering
		for (Texture t: atlas.getTextures()) {
			t.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		}
		
		// Create game resource objects
		fonts = new AssetFonts();
		sounds = new AssetSounds(assetManager);
		music = new AssetMusic(assetManager);
		agent= new AssetAgent(atlas);
		levels = new AssetLevels(atlas);
		solidColors = new AssetSolid(atlas);
	}

	@Override
	public void dispose() {
		assetManager.dispose();
		fonts.defaultSmall.dispose();
		fonts.defaultNormal.dispose();
		fonts.defaultBig.dispose();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void error(AssetDescriptor asset, Throwable throwable) {
		Gdx.app.log(TAG, "Couldn't load asset '" + asset.fileName + "'", (Exception) throwable);
	}

}
