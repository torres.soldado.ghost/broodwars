package com.hexperiments.broodwars.game.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.hexperiments.broodwars.game.Assets;
import com.hexperiments.broodwars.map.GameMapData;
import com.hexperiments.broodwars.map.Node;
import com.hexperiments.broodwars.utils.Constants;
import com.sun.corba.se.impl.orbutil.closure.Constant;

public class Brood implements Runnable {
	public static final String TAG = Brood.class.getName();
	Broodling[] broodlings;
	GameMapData mapRef;
	public int targX;
	public int targY;
	float colorR, colorG, colorB;
	ShapeRenderer shapeRenderer;
	
	public void setTargetCoordinates(int x, int y) {
		targX = x;
		targY = y;
	}
	
	public Brood(GameMapData map, int x, int y, int n, float R, float G, float B) {
		this.mapRef = map;
		colorR = R;
		colorG = G;
		colorB = B;
		Gdx.app.debug(TAG, "Allocating broodling array.");
		broodlings = new Broodling[n];
		Gdx.app.debug(TAG, "Allocating broodling array done.");
		Gdx.app.debug(TAG, "Spawning broodlings.");
		/*
		 * spawn zerglings in free areas at x,y.
		 * ToDo: Spawn zerglings in free spaces or define a map zone to spawn the zerglings.
		 * 5 * 5 tiles = 50 * 50 cells = 2500 zerglings. 
		 */
//		int id = 1;
//		int len = (int) (Math.sqrt(n) + 1);
//		int xLim = x + len;
//		int yLim = y + len;
//		for (int xx = x; xx <= xLim; ++xx) {
//			for (int yy = y; yy <= yLim; ++yy) {
//				map.setMinionId(xx, yy, id);
//				Broodling broodling = new Broodling(xx, yy, id);
//				broodlings[id - 1] = broodling;
//				id++;
//				if (id >= n + 1)
//					break;
//			}
//			if (id >= n + 1)
//				break;
//		}
		int id = 1;
		for (int xx = x; xx <= 800; xx = xx + 1) {
			for (int yy = y; yy <= 480; yy = yy + 1) {
				if (map.isCellFree(xx, yy)) {
					map.setMinionId(xx, yy, id);
					Broodling broodling = new Broodling(xx, yy, id);
					broodlings[id - 1] = broodling;
					id++;
					if (id >= n + 1)
						break;
				}
			}
			if (id >= n + 1)
				break;
		}
		Gdx.app.debug(TAG, "Spawning broodlings done.");
		
		targX = 400;
		targY = 240;
		
		shapeRenderer = new ShapeRenderer();
	}
	
	@SuppressWarnings("unused")
	private void updateBroodling0(Broodling broodling) {
		// decide which tile to "jump" to. ToDo: Implement smooth moving??
		int deltaX = targX - broodling.x;
		int deltaY = targY - broodling.y;
		int deltaXAbs = Math.abs(deltaX);
		int deltaYAbs = Math.abs(deltaY);
		
		int speedXOptimal = deltaX < 0? -1: 1;
//		if (deltaX == 0)
//			speedXOptimal = 0;
		int speedYOptimal = deltaY < 0? -1: 1;
//		if (deltaY == 0)
//			speedYOptimal = 0;
		
		int speedXGood = deltaXAbs > deltaYAbs? speedXOptimal: 0;
		int speedYGood = deltaXAbs > deltaYAbs? 0: speedYOptimal;
		
		int speedXAcceptable = speedYGood;
		int speedYAcceptable = speedXGood;
		
		if (mapRef.isCellFree(broodling.x + speedXOptimal, broodling.y + speedYOptimal)) {
			// Optimal direction
			mapRef.clearMinionId(broodling.x, broodling.y);
			broodling.x += speedXOptimal;
			broodling.y += speedYOptimal;
			mapRef.setMinionId(broodling.x, broodling.y, broodling.id);
		} else if (mapRef.isCellFree(broodling.x + speedXGood, broodling.y + speedYGood)) {
			// Good direction
			mapRef.clearMinionId(broodling.x, broodling.y);
			broodling.x += speedXGood;
			broodling.y += speedYGood;
			mapRef.setMinionId(broodling.x, broodling.y, broodling.id);
		} else if (mapRef.isCellFree(broodling.x + speedXAcceptable, broodling.y + speedYAcceptable)) {
			// Acceptable direction
			mapRef.clearMinionId(broodling.x, broodling.y);
			broodling.x += speedXAcceptable;
			broodling.y += speedYAcceptable;
			mapRef.setMinionId(broodling.x, broodling.y, broodling.id);
		}
	}
	
	@SuppressWarnings("unused")
	private void updateBroodling1(Broodling broodling) {
		// decide which tile to "jump" to. ToDo: Implement smooth moving??		
		int distMin = Integer.MAX_VALUE;
		int x = broodling.x;
		int y = broodling.y;
		for (int yy = broodling.y - 1; yy <= broodling.y + 1; ++yy) {
			for (int xx = broodling.x - 1; xx <= broodling.x + 1; ++xx) {
				if ((xx != broodling.x) && (yy != broodling.y) && mapRef.isCellFree(xx, yy)) {
					int tmpDist = Math.abs(targX - xx) + Math.abs(targY- yy);
					if (tmpDist < distMin) {
						distMin = tmpDist;
						x = xx;
						y = yy;
					}
				}
			}
		}
		
		mapRef.clearMinionId(broodling.x, broodling.y);
		broodling.x = x;
		broodling.y = y;
		mapRef.setMinionId(broodling.x, broodling.y, broodling.id);
	}
	
	@SuppressWarnings("unused")
	private void updateBroodling2(Broodling broodling) {
		// decide which tile to "jump" to. ToDo: Implement smooth moving??		
		int distMin = Integer.MAX_VALUE;
		int x = broodling.x;
		int y = broodling.y;
		boolean decided = false;
			
		int deltaX = targX- broodling.x;
		int deltaY = targY - broodling.y;		
		int speedXOptimal = deltaX < 0? -1: 1;
		if (deltaX == 0)
			speedXOptimal = 0;
		int speedYOptimal = deltaY < 0? -1: 1;
		if (deltaY == 0)
			speedYOptimal = 0;
		int xx = broodling.x + speedXOptimal;
		int yy = broodling.y + speedYOptimal;
		if (xx >= 0 && xx < mapRef.width && yy > 0 && yy < mapRef.height) {
			int fullId = mapRef.cells[xx][yy];
			if (fullId != GameMapData.CELL_INVALID) {
				int idPushed;
				if (xx > broodling.x) {
					// push him to the right
					idPushed = fullId | (1 << 32);
				} else {
					idPushed = fullId | (1 << 31);
				}
				if (yy > broodling.y) {
					// push him to the top
					idPushed = fullId | (1 << 30);
				} else {
					idPushed = fullId | (1 << 29);
				}
				mapRef.setMinionId(xx, yy, idPushed );
			}
		}
		
		// check if we were pushed try and get out of the way
		if ((mapRef.getMinionId(x, y) & (1 << 32)) != 0) {
			// we were pushed to the right
			if (mapRef.isCellFree(broodling.x + 1, broodling.y)) {
				x = broodling.x + 1;
				decided = true;
			}
		}
		if (!decided && (mapRef.getMinionId(x, y) & (1 << 31)) != 0) {
			// we were pushed to the left
			if (mapRef.isCellFree(broodling.x - 1, broodling.y)) {
				x = broodling.x - 1;
				decided = true;
			}
		}
		if (!decided && (mapRef.getMinionId(x, y) & (1 << 30)) != 0) {
			// we were pushed up
			if (mapRef.isCellFree(broodling.x, broodling.y + 1)) {
				y = broodling.y + 1;
				decided = true;
			}
		}
		if (!decided && (mapRef.getMinionId(x, y) & (1 << 29)) != 0) {
			// we were pushed down
			if (mapRef.isCellFree(broodling.x, broodling.y - 1)) {
				y = broodling.y - 1;
				decided = true;
			}
		}
		
		if (!decided) {
			for (yy = broodling.y - 1; yy <= broodling.y + 1; ++yy) {
				for (xx = broodling.x - 1; xx <= broodling.x + 1; ++xx) {
					if ((xx != broodling.x) && (yy != broodling.y)) { 
						if (mapRef.isCellFree(xx, yy)) {
							int tmpDist = Math.abs(targX - xx) + Math.abs(targY - yy);
							if (tmpDist < distMin) {
								distMin = tmpDist;
								x = xx;
								y = yy;
							}
						}
					}
				}
			}
		}
		
		mapRef.clearMinionId(broodling.x, broodling.y);
		broodling.x = x;
		broodling.y = y;
		mapRef.setMinionId(broodling.x, broodling.y, broodling.id);
	}
	
	@SuppressWarnings("unused")
	private void updateBroodling3(Broodling broodling) {
		int targetX, targetY;
		int curNodeIdx = mapRef.getNodeId(broodling.x, broodling.y) - 1;
		int targNodeIdx = mapRef.cells[targX][targY] - 1;
		
		// check if the target node is the current node
		if (curNodeIdx != targNodeIdx) {
			Node nextNode = mapRef.nodes.get(curNodeIdx).next;
			targetX = nextNode.centerX;
			targetY = nextNode.centerY;
		} else {
			targetX = targX;
			targetY = targY;
		}
		
		// decide which tile to "jump" to. ToDo: Implement smooth moving??		
		int distMin = Integer.MAX_VALUE;
		int x = broodling.x;
		int y = broodling.y;
		for (int yy = broodling.y - 1; yy <= broodling.y + 1; ++yy) {
			for (int xx = broodling.x - 1; xx <= broodling.x + 1; ++xx) {
				if ((xx != broodling.x) && (yy != broodling.y) && mapRef.isCellFree(xx, yy)) {
					int tmpDist = Math.abs(targetX- xx) + Math.abs(targetY - yy);
					if ((xx == broodling.x + 1) || (xx == broodling.x - 1) && 
						(yy == broodling.y + 1) || (yy == broodling.y - 1))
						tmpDist = (int) (tmpDist * 1.4);
					if (tmpDist < distMin) {
						distMin = tmpDist;
						x = xx;
						y = yy;
					}
				}
			}
		}
		
		mapRef.clearMinionId(broodling.x, broodling.y);
		broodling.x = x;
		broodling.y = y;
		mapRef.setMinionId(broodling.x, broodling.y, broodling.id);
	}
	
	@SuppressWarnings("unused")
	private void updateBroodling4(Broodling broodling) {
		// decide which tile to "jump" to. ToDo: Implement smooth moving??		
		int distMin = Integer.MAX_VALUE;
		int x = broodling.x;
		int y = broodling.y;
		for (int yy = broodling.y - 1; yy <= broodling.y + 1; ++yy) {
			for (int xx = broodling.x - 1; xx <= broodling.x + 1; ++xx) {
				if ((xx != broodling.x) && (yy != broodling.y) && mapRef.isCellFree(xx, yy)) {
					int tmpDist = Math.abs(targX- xx) + Math.abs(targY - yy);
					if ((xx == broodling.x + 1) || (xx == broodling.x - 1) && 
						(yy == broodling.y + 1) || (yy == broodling.y - 1))
						tmpDist = (int) (tmpDist * 1.4);
					if (tmpDist < distMin) {
						distMin = tmpDist;
						x = xx;
						y = yy;
					}
				}
			}
		}
		
		mapRef.clearMinionId(broodling.x, broodling.y);
		broodling.x = x;
		broodling.y = y;
		mapRef.setMinionId(broodling.x, broodling.y, broodling.id);
	}
	
	@SuppressWarnings("unused")
	private void updateBroodling5(Broodling broodling) {
		int targetX, targetY;
		int curNodeIdx = mapRef.getNodeId(broodling.x, broodling.y) - 1;
		int targNodeIdx = mapRef.getNodeId(targX, targY) - 1;
		
		// check if the target node is the current node
		if (curNodeIdx != targNodeIdx) {
			Node nextNode = mapRef.nodes.get(curNodeIdx).next;
			targetX = nextNode.centerX;
			targetY = nextNode.centerY;
			
			if (nextNode.x < broodling.x && (nextNode.x + nextNode.w) > broodling.x) {
				targetX = broodling.x;
			}
			if (nextNode.y < broodling.y && (nextNode.y + nextNode.w) > broodling.y) {
				targetY = broodling.y;
			}
		} else {
			targetX = targX;
			targetY = targY;
		}
		
		// decide which tile to "jump" to. ToDo: Implement smooth moving??		
		int distMin = Integer.MAX_VALUE;
		int x = broodling.x;
		int y = broodling.y;
		if (targetX == broodling.x && targetY == broodling.y)
			return;
		
		// optimal direction
		if (targetX >= x && targetY >= y) {
			if (mapRef.isCellFree(x + 1, y + 1)) {
				x++;
				y++;
			} else if (mapRef.isCellFree(x, y + 1)) {
				y++;
			} else if (mapRef.isCellFree(x + 1, y)) {
				x++;
			} else if (mapRef.isCellFree(x - 1, y + 1)) {
				x--;
				y++;
			} else if (mapRef.isCellFree(x + 1, y - 1)) {
				x++;
				y--;
			}
		} else if (targetX <= x && targetY <= y) {
			if (mapRef.isCellFree(x - 1, y - 1)) {
				x--;
				y--;
			} else if (mapRef.isCellFree(x, y - 1)) {
				y--;
			} else if (mapRef.isCellFree(x - 1, y)) {
				x--;
			} else if (mapRef.isCellFree(x + 1, y - 1)) {
				x++;
				y--;
			} else if (mapRef.isCellFree(x - 1, y + 1)) {
				x--;
				y++;
			}
		} else if (targetX >= x && targetY <= y) {
			if (mapRef.isCellFree(x + 1, y - 1)) {
				x++;
				y--;
			} else if (mapRef.isCellFree(x, y - 1)) {
				y--;
			} else if (mapRef.isCellFree(x + 1, y)) {
				x++;
			} else if (mapRef.isCellFree(x + 1, y - 1)) {
				x++;
				y--;
			} else if (mapRef.isCellFree(x - 1, y - 1)) {
				x--;
				y--;
			}
		} else if (targetX <= x && targetY >= y) {
			if (mapRef.isCellFree(x - 1, y + 1)) {
				x--;
				y++;
			} else if (mapRef.isCellFree(x, y + 1)) {
				y++;
			} else if (mapRef.isCellFree(x - 1, y)) {
				x--;
			} else if (mapRef.isCellFree(x + 1, y + 1)) {
				x++;
				y++;
			} else if (mapRef.isCellFree(x - 1, y - 1)) {
				x--;
				y--;
			}
		}
		
		// jump to next position
		mapRef.clearMinionId(broodling.x, broodling.y);
		broodling.x = x;
		broodling.y = y;
		mapRef.setMinionId(broodling.x, broodling.y, broodling.id);
	}
	
	private void updateBroodlingPos(Broodling broodling, int nextX, int nextY) {
		// jump to next position
		if (nextX > broodling.x) {
			broodling.prevIncX = 1;
			broodling.prevIncY = 0;
		} else if (nextX < broodling.x) {
			broodling.prevIncX = -1;
			broodling.prevIncY = 0;
		} else if (nextY > broodling.y) {
			broodling.prevIncX = 0;
			broodling.prevIncY = 1;
		} else {
			broodling.prevIncX = 0;
			broodling.prevIncY = -1;
		}
		mapRef.clearMinionId(broodling.x, broodling.y);
		broodling.x = nextX;
		broodling.y = nextY;
		mapRef.setMinionId(broodling.x, broodling.y, broodling.id);
	}
	
	@SuppressWarnings("unused")
	private void updateBroodling6(Broodling broodling, boolean randBool) {		
		final int SPEED = 4;
		
		int targetX, targetY;
		int curNodeIdx = mapRef.getNodeId(broodling.x, broodling.y) - 1;
		int targNodeIdx = mapRef.getNodeId(targX, targY) - 1;
		
		// check if the target node is the current node
		if (curNodeIdx != targNodeIdx) {
			Node nextNode = mapRef.nodes.get(curNodeIdx).next;
			targetX = nextNode.centerX;
			targetY = nextNode.centerY;
			
			if (nextNode.x < broodling.x && (nextNode.x + nextNode.w) > broodling.x) {
				targetX = broodling.x;
			}
			if (nextNode.y < broodling.y && (nextNode.y + nextNode.w) > broodling.y) {
				targetY = broodling.y;
			}
		} else {
			targetX = targX;
			targetY = targY;
		}
		
		int deltaX = targetX - broodling.x;
		int deltaY = targetY - broodling.y;
		int deltaXAbs = Math.abs(deltaX);
		int deltaYAbs = Math.abs(deltaY);
		int deltaDeltas = deltaXAbs - deltaYAbs;
		int incX = 0;
		int incY = 0;
		
		// try and follow optimal direction
		if (deltaX > 0) {
			incX = SPEED;
		} else if (deltaX < 0) {
			incX = -SPEED;
		}
		
		if (deltaY > 0) {
			incY = SPEED;
		} else if (deltaY < 0) {
			incY = -SPEED;
		}
		if (mapRef.isCellFree(broodling.x + incX, broodling.y + incY)) {
			updateBroodlingPos(broodling, broodling.x + incX, broodling.y + incY);
			return;
		}
		
		// try and surround target
		if (deltaX < 0) {
			// top right corner
			if (deltaX == deltaY) {
				if (broodling.clockWise) {
					if (mapRef.isCellFree(broodling.x, broodling.y - SPEED)) {
						updateBroodlingPos(broodling, broodling.x, broodling.y - SPEED);
						return;						
					} else if (mapRef.isCellFree(broodling.x - SPEED, broodling.y)) {
						broodling.clockWise = false;
						updateBroodlingPos(broodling, broodling.x - SPEED, broodling.y);
						return;	
					}
				} else {
					if (mapRef.isCellFree(broodling.x - SPEED, broodling.y)) {
						updateBroodlingPos(broodling, broodling.x - SPEED, broodling.y);
						return;	
					} else if (mapRef.isCellFree(broodling.x, broodling.y - SPEED)) {
						broodling.clockWise = true;
						updateBroodlingPos(broodling, broodling.x, broodling.y - SPEED);
						return;						
					}
				}
			}
			
			// bottom right corner
			if (deltaX == -deltaY) {
				if (broodling.clockWise) {
					if (mapRef.isCellFree(broodling.x - SPEED, broodling.y)) {
						updateBroodlingPos(broodling, broodling.x - SPEED, broodling.y);
						return;						
					} else if (mapRef.isCellFree(broodling.x, broodling.y + SPEED)) {
						broodling.clockWise = false;
						updateBroodlingPos(broodling, broodling.x, broodling.y + SPEED);
						return;	
					}
				} else {
					if (mapRef.isCellFree(broodling.x, broodling.y + SPEED)) {
						updateBroodlingPos(broodling, broodling.x, broodling.y + SPEED);
						return;	
					} else if (mapRef.isCellFree(broodling.x - SPEED, broodling.y)) {
						broodling.clockWise = true;
						updateBroodlingPos(broodling, broodling.x - SPEED, broodling.y);
						return;						
					}
				}
			}
			
			// inner right
			if (deltaXAbs > deltaYAbs) {
				if (broodling.clockWise) {
					if (mapRef.isCellFree(broodling.x, broodling.y - SPEED)) {
						updateBroodlingPos(broodling, broodling.x, broodling.y - SPEED);
						return;						
					} else if (mapRef.isCellFree(broodling.x, broodling.y + SPEED)) {
						broodling.clockWise = false;
						updateBroodlingPos(broodling, broodling.x, broodling.y + SPEED);
						return;	
					}
				} else {
					if (mapRef.isCellFree(broodling.x, broodling.y + SPEED)) {
						updateBroodlingPos(broodling, broodling.x, broodling.y + SPEED);
						return;	
					} else if (mapRef.isCellFree(broodling.x, broodling.y - SPEED)) {
						broodling.clockWise = true;
						updateBroodlingPos(broodling, broodling.x, broodling.y - SPEED);
						return;						
					}
				}
			}
		}
	
	
		if (deltaX > 0) {
			// bottom left corner
			if (deltaX == deltaY) {
				if (broodling.clockWise) {
					if (mapRef.isCellFree(broodling.x, broodling.y + SPEED)) {
						updateBroodlingPos(broodling, broodling.x, broodling.y + SPEED);
						return;						
					} else if (mapRef.isCellFree(broodling.x + SPEED, broodling.y)) {
						broodling.clockWise = false;
						updateBroodlingPos(broodling, broodling.x + SPEED, broodling.y);
						return;	
					}
				} else {
					if (mapRef.isCellFree(broodling.x + SPEED, broodling.y)) {
						updateBroodlingPos(broodling, broodling.x + SPEED, broodling.y);
						return;	
					} else if (mapRef.isCellFree(broodling.x, broodling.y + SPEED)) {
						broodling.clockWise = true;
						updateBroodlingPos(broodling, broodling.x, broodling.y + SPEED);
						return;						
					}
				}
			}
			
			// top left corner
			if (deltaX == -deltaY) {
				if (broodling.clockWise) {
					if (mapRef.isCellFree(broodling.x + SPEED, broodling.y)) {
						updateBroodlingPos(broodling, broodling.x + SPEED, broodling.y);
						return;						
					} else if (mapRef.isCellFree(broodling.x, broodling.y - SPEED)) {
						broodling.clockWise = false;
						updateBroodlingPos(broodling, broodling.x, broodling.y - SPEED);
						return;	
					}
				} else {
					if (mapRef.isCellFree(broodling.x, broodling.y - SPEED)) {
						updateBroodlingPos(broodling, broodling.x, broodling.y - SPEED);
						return;	
					} else if (mapRef.isCellFree(broodling.x + SPEED, broodling.y)) {
						broodling.clockWise = true;
						updateBroodlingPos(broodling, broodling.x + SPEED, broodling.y);
						return;						
					}
				}
			}
			
			// inner left
			if (deltaXAbs > deltaYAbs) {
				if (broodling.clockWise) {
					if (mapRef.isCellFree(broodling.x, broodling.y + SPEED)) {
						updateBroodlingPos(broodling, broodling.x, broodling.y + SPEED);
						return;						
					} else if (mapRef.isCellFree(broodling.x, broodling.y - SPEED)) {
						broodling.clockWise = false;
						updateBroodlingPos(broodling, broodling.x, broodling.y - SPEED);
						return;	
					}
				} else {
					if (mapRef.isCellFree(broodling.x, broodling.y - SPEED)) {
						updateBroodlingPos(broodling, broodling.x, broodling.y - SPEED);
						return;	
					} else if (mapRef.isCellFree(broodling.x, broodling.y + SPEED)) {
						broodling.clockWise = true;
						updateBroodlingPos(broodling, broodling.x, broodling.y + SPEED);
						return;						
					}
				}
			}
		}
			
		if (deltaY < 0) {
			if (deltaYAbs > deltaXAbs) {
				if (broodling.clockWise) {
					if (mapRef.isCellFree(broodling.x + SPEED, broodling.y)) {
						updateBroodlingPos(broodling, broodling.x + SPEED, broodling.y);
						return;						
					} else if (mapRef.isCellFree(broodling.x - SPEED, broodling.y)) {
						broodling.clockWise = false;
						updateBroodlingPos(broodling, broodling.x - SPEED, broodling.y);
						return;	
					}
				} else {
					if (mapRef.isCellFree(broodling.x - SPEED, broodling.y)) {
						updateBroodlingPos(broodling, broodling.x - SPEED, broodling.y);
						return;	
					} else if (mapRef.isCellFree(broodling.x + SPEED, broodling.y)) {
						broodling.clockWise = true;
						updateBroodlingPos(broodling, broodling.x + SPEED, broodling.y);
						return;						
					}
				}
			}
		}
		
		if (deltaY > 0) {
			if (deltaYAbs > deltaXAbs) {
				if (broodling.clockWise) {
					if (mapRef.isCellFree(broodling.x - SPEED, broodling.y)) {
						updateBroodlingPos(broodling, broodling.x - SPEED, broodling.y);
						return;						
					} else if (mapRef.isCellFree(broodling.x + SPEED, broodling.y)) {
						broodling.clockWise = false;
						updateBroodlingPos(broodling, broodling.x + SPEED, broodling.y);
						return;	
					}
				} else {
					if (mapRef.isCellFree(broodling.x + SPEED, broodling.y)) {
						updateBroodlingPos(broodling, broodling.x + SPEED, broodling.y);
						return;	
					} else if (mapRef.isCellFree(broodling.x - SPEED, broodling.y)) {
						broodling.clockWise = true;
						updateBroodlingPos(broodling, broodling.x - SPEED, broodling.y);
						return;						
					}
				}
			}
		}
		
		return;
	}
	
	float time = 0;
	public void update(float deltaTime) {
		boolean randBool = ((int)deltaTime & 1) != 0;
		time += deltaTime;
		if (time >= 0.000f) {
			time = 0;
			long startTime;
			if (Constants.DEBUG_ENABLED)
				startTime = System.nanoTime();
			for (Broodling broodling: broodlings) {
				updateBroodling6(broodling, randBool);
			}
			if (Constants.DEBUG_ENABLED) {
				long endTime = System.nanoTime();
				long duration = endTime - startTime;
				Gdx.app.debug(TAG, "DeltaT brood update:" + (float)(duration / 1000000000.0f));
			}
		}
	}
	
	public void render(SpriteBatch batch) {
		long startTime;
		if (Constants.DEBUG_ENABLED)
			startTime = System.nanoTime();

//		batch.enableBlending();
		batch.setColor(colorR, colorG, colorB, 0.9f);
		for(Broodling broodling: broodlings) {
//			batch.draw(Assets.instance.solidColors.black, broodling.x, broodling.y);
			batch.draw(Assets.instance.agent.agent, 
					broodling.x - 5, broodling.y - 5,
					0, 0,
					10, 10,
					1, 1,
					0);
//			batch.draw(region, x, y, originX, originY, width, height, scaleX, scaleY, rotation);
		}
//		batch.disableBlending();
		batch.setColor(1, 1, 1, 1);

		if (Constants.DEBUG_ENABLED) {
			long endTime = System.nanoTime();
			long duration = endTime - startTime;
			Gdx.app.debug(TAG, "DeltaT draw brood:" + (float)(duration / 1000000000.0f));
		}
	}

	@Override
	public void run() {
		while(true) {
			try {
				Thread.sleep(20);
				this.update(1.0f);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
//	/**
//	 * Spawn a minion to the closest free and valid position to (x,y). THIS IS TOO SLOW!
//	 * 
//	 * @param x
//	 * @param y
//	 * @param id
//	 * @return True if successful, False otherwise.
//	 */
//	public int[] spawnMinion(int x, int y, int id) {
//		int retPos[] = new int[2];
//		retPos[0] = -1;
//		retPos[1] = -1;
//			
//		int maxRadius = Math.max(Math.max(y, height - y), Math.max(x, width - x));
//		for (int i = 0; i < maxRadius; ++i) {
//			for (int yIdx = y + i; yIdx >= y - i; --yIdx) {
//				for (int xIdx = x + i; xIdx >= x - i; --xIdx) {
//					if (isCellFree(xIdx, yIdx)) {
//						setMinionId(xIdx, yIdx, id);
//						retPos[0] = xIdx;
//						retPos[1] = yIdx;
//						return retPos;
//					}
//				}
//			}
//		}
//		
//		return retPos;
//	}
}
