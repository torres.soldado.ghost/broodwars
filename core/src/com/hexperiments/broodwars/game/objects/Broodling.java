package com.hexperiments.broodwars.game.objects;

public class Broodling {
	public static final String TAG = Broodling.class.getName();
	
	public int xTarg;
	public int yTarg;
	public int x;
	public int y;
	public int prevIncX;
	public int prevIncY;
	public boolean clockWise;
	public int id;
	
	public Broodling(int x, int y, int id) {
		// init parameters
		this.xTarg = 400;
		this.yTarg = 240;
		this.x = x;
		this.y = y;
		this.id = id;
		this.clockWise = true;
	}
}
