package com.hexperiments.broodwars.map;

public class PreNode {
	public static final String TAG = PreNode.class.getName(); // for debugging
	private static final int NW = 0; // North-West
	private static final int SW = 1; // South-West
	private static final int SE = 2; // South-East 
	private static final int NE = 3; // North-East
	private static final int OM = 4; // Omni directional
	
	public int x; // x origin if tile
	public int y; // y origin if tile
	public int size; // Size in sub-nodes/tiles. Mainly to compare with other nodes when building mesh.
	public int left; // x coordinate of left edge
	public int right; // x coordinate of right edge
	public int bottom; // y coordinate of top edge
	public int top; // y coordinate of bottom edge
	public int id;
	public int drawX;
	public int drawY;
	public int drawWidth;
	public int drawHeight;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public void init(int originX, int originY, int id) {
		x = originX;
		y = originY;
		size = 0;
		left = 0;
		right = 0;
		bottom = 0;
		top = 0;
		this.id = id;
	}
	
	/**
	 * Mark node's occupation on map with node's id.
	 */
	public void markMap(int map[][]) {
		for (int x = left; x <= right; ++x) {
			for (int y = bottom; y <= top; ++y) {
				map[x][y] = id;
			}
		}
	}
	
	public boolean isBiggerThan(PreNode node) {
		return size > node.size;
	}
	
	public void copyFrom(PreNode node) {
		x = node.x;
		y = node.y;
		size = node.size;
		left = node.left;
		right = node.right;
		bottom = node.bottom;
		top = node.top;
		id = node.id;
		drawX = node.drawX;
		drawY = node.drawY;
		drawWidth = node.drawWidth;
		drawHeight = node.drawHeight;
	}
	
	/**
	 * See how much the current node can grow. Checks the adjacent nodes to see if they are clear,
	 * then tries to grow in each direction maintaining a square occupation shape. Growing here means
	 * that a node can be composed of n^2 tiles, n = 1, 2, 3, ...
	 */
	public void grow(int map[][], int mapWidth, int mapHeight) {
		// get maximum growth units in each direction
		int growUnitsNW = growNorthWest(map, mapWidth, mapHeight);
		int growUnitsSW = growSouthWest(map, mapWidth, mapHeight);
		int growUnitsSE = growSouthEast(map, mapWidth, mapHeight);
		int growUnitsNE = growNorthEast(map, mapWidth, mapHeight);
		int growUnitsAll = Math.min(Math.min(growUnitsNW, growUnitsSW),Math.min(growUnitsSE, growUnitsNE)); 

		// calculate the number of tiles
		int growSizes[] = new int[5];
		// NW
		growSizes[NW] = (growUnitsNW + 1) * (growUnitsNW + 1);
		// SW
		growSizes[SW] = (growUnitsSW + 1) * (growUnitsSW + 1);
		// SE
		growSizes[SE] = (growUnitsSE + 1) * (growUnitsSE + 1);
		// NE
		growSizes[NE] = (growUnitsNE + 1) * (growUnitsNE + 1);
		// omni
		growSizes[OM] = (1 + growUnitsAll * 2) * (1 + growUnitsAll * 2); 

		// get direction of biggest growth
		size = growSizes[0];
		int idx = 0;
		for (int i = 1; i < growSizes.length; ++i) {
			if (growSizes[i] > size) {
				size = growSizes[i];
				idx = i;
			}
		}
		
		// set node geometry bounds
		switch (idx) {
		case NW:
			bottom = y;
			left = x - growUnitsNW;
			top = y + growUnitsNW;
			right = x;
			break;
		case SW:
			bottom = y - growUnitsSW;
			left = x - growUnitsSW;
			top = y;
			right = x;
			break;
		case SE:
			bottom = y - growUnitsSE;
			left = x;
			top = y;
			right = x + growUnitsSE;
			break;
		case NE:
			bottom = y;
			left = x;
			top = y + growUnitsNE;
			right = x + growUnitsNE;
			break;
		case OM:
			bottom = y - growUnitsAll;
			left = x - growUnitsAll;
			top = y + growUnitsAll;
			right = x + growUnitsAll;
			break;
		}
		
		drawX = left * 10;
		drawY = bottom * 10;
		drawWidth = (right - left + 1) * 10;
		drawHeight = (top - bottom + 1) * 10;
		if (drawWidth < 0) {
			drawX -= 2;
			drawWidth += 4;
		} else {
			drawX += 2;
			drawWidth -= 4;
		}
		if (drawHeight < 0) {
			drawY -= 2;
			drawHeight += 4;
		} else {
			drawY += 2;
			drawHeight -= 4;
		}
	}
	
	private int growNorthWest(int map[][], int mapWidth, int mapHeight) {
		int maxUnits = Math.min(x, mapHeight - y - 1); // maximum size of square from origin to direction
		int nExpUnits = 0;
		int yIdx;
		int xIdx;
		for (int i = 1; i < maxUnits; ++i) {
			// top
			yIdx = y + i;
			for (xIdx = x; xIdx >= x - i; --xIdx) {
				if (map[xIdx][yIdx] != 0) { // Tile unavailable
					return nExpUnits > 0 ? nExpUnits : 0;
				}
			}
			
			// left
			xIdx = x - i;
			for (yIdx = y; yIdx <= y + i; ++yIdx) {
				if (map[xIdx][yIdx] != 0) { // Tile unavailable
					return nExpUnits > 0 ? nExpUnits : 0;
				}
			}
			
			nExpUnits++;
		}
		
		return nExpUnits > 0 ? nExpUnits : 0;
	}
	
	int growSouthWest(int map[][], int mapWidth, int mapHeight) {
		int maxUnits = Math.min(x, y); // maximum size of square from origin to direction
		int nExpUnits = 0;
		int yIdx;
		int xIdx;
		for (int i = 1; i < maxUnits; ++i) {
			// bottom
			yIdx = y - i;
			for (xIdx = x; xIdx >= x - i; --xIdx) {
				if (map[xIdx][yIdx] != 0) { // Tile unavailable
					return nExpUnits > 0 ? nExpUnits : 0;
				}
			}
			
			// left
			xIdx = x - i;
			for (yIdx = y; yIdx >= y - i; --yIdx) {
				if (map[xIdx][yIdx] != 0) { // Tile unavailable
					return nExpUnits > 0 ? nExpUnits : 0;
				}
			}
			
			nExpUnits++;
		}
		
		return nExpUnits > 0 ? nExpUnits : 0;
	}
	
	int growSouthEast(int map[][], int mapWidth, int mapHeight) {
		int maxUnits = Math.min(mapWidth - x - 1, y); // maximum size of square from origin to direction
		int nExpUnits = 0;
		int yIdx;
		int xIdx;
		for (int i = 1; i < maxUnits; ++i) {
			// bottom
			yIdx = y - i;
			for (xIdx = x; xIdx <= x + i; ++xIdx) {
				if (map[xIdx][yIdx] != 0) { // Tile unavailable
					return nExpUnits > 0 ? nExpUnits : 0;
				}
			}
			
			// right
			xIdx = x + i;
			for (yIdx = y; yIdx >= y - i; --yIdx) {
				if (map[xIdx][yIdx] != 0) { // Tile unavailable
					return nExpUnits > 0 ? nExpUnits : 0;
				}
			}
			
			nExpUnits++;
		}
		
		return nExpUnits > 0 ? nExpUnits : 0;
	}
	
	int growNorthEast(int map[][], int mapWidth, int mapHeight) {
		int maxUnits = Math.min(mapWidth - x - 1, mapHeight - y - 1); // maximum size of square from origin to direction
		int nExpUnits = 0;
		int yIdx;
		int xIdx;
		for (int i = 1; i < maxUnits; ++i) {
			// top
			yIdx = y + i;
			for (xIdx = x; xIdx <= x + i; ++xIdx) {
				if (map[xIdx][yIdx] != 0) { // Tile unavailable
					return nExpUnits > 0 ? nExpUnits : 0;
				}
			}
			
			// right
			xIdx = x + i;
			for (yIdx = y; yIdx <= y + i; ++yIdx) {
				try {
					if (map[xIdx][yIdx] != 0) { // Tile unavailable
						return nExpUnits > 0 ? nExpUnits : 0;
					}
				} catch (ArrayIndexOutOfBoundsException e) {
					System.out.println("Array index OOB: x:" + x + " y:" + y);
				}
			}
			
			nExpUnits++;
		}
		
		return nExpUnits > 0 ? nExpUnits : 0;
	}
}
