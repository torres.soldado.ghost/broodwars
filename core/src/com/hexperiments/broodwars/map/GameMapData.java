package com.hexperiments.broodwars.map;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;

public class GameMapData {
	public static final String TAG = GameMapData.class.getName();
	public ArrayList<Node> nodes;
	public static final int CELL_INVALID_NODE = 0xFFF;
	public static final int CELL_INVALID_MINION = 0xFFFFF;
	public static final int CELL_INVALID = 0xFFFFFFFF; // For walls
	public static final int CELL_FREE_NODE = 0;
	public static final int CELL_FREE_MINION = 0;
	
	public int[][] cells; /* Holds ids of nodes in bits [11 - 0] (12 bits - 4,096) 
						and occupying minion on bits [32 - 12] (20 bits - 1,048,575) */
	public int width;
	public int height;
	
	/**
	 * Get the id of the minion that is occupying the specific cell.
	 * @param x: Cell x coordinates.
	 * @param y: Cell y coordinates.
	 * @return Id of minion, CELL_FREE_MINION if no minion.
	 */
	public int getMinionId(int x, int y) { // No x, y verification because of speed
		return cells[x][y] >> 12;
//		return cells[x][y] & 0xFFFFF000;
	}
	
	/**
	 * Get the id(index) of the node of which the specified cell belongs to.
	 * @param x: Cell x coordinates.
	 * @param y: Cell y coordinates.
	 * @return Id of node.
	 */
	public int getNodeId(int x, int y) {
		return cells[x][y] & 0xFFF;
	}
	
	/**
	 * Set the minion id of the specified cell.
	 * @param x: Cell x coordinate.
	 * @param y: Cell y coordinate.
	 * @param id: Id of minion.
	 */
	public void setMinionId(int x, int y, int id) { // No x, y verification because of speed
		cells[x][y] &=  0xFFF; // Clear minion id bits
		cells[x][y] |= id << 12;
	}
	
	/**
	 * Clears the minion id of the specified cell.
	 * @param x: Cell x coordinate.
	 * @param y: Cell y coordinate.
	 */
	public void clearMinionId(int x, int y) {
		cells[x][y] &= 0xFFF;
	}
	
	/**
	 * Check wether a cell is free, i.e. there is no minion on it.
	 * @param x: Cell x coordinate.
	 * @param y: Cell y coordinate.
	 * @return True if there is no minion associated with the cell, False otherwise.
	 */
	public boolean isCellFree(int x, int y) {
		if (x < 0 || x >= width || y < 0 || y >= height)
			return false;
//		if (cells[x][y] == CELL_INVALID)
//			return false;
		if (getMinionId(x, y) != CELL_FREE_MINION)
			return false;
		
		return true;
	}
	
	// level tile types
	public enum PIXEL_TYPE {
		EMPTY(255, 255, 255),
		WALL(0, 0, 0);	
	
		private int color;
	
		private PIXEL_TYPE (int r, int g, int b) {
			color = r << 24 | g << 16 | b << 8 | 0xFF;
		}
		
		public boolean sameColor(int color) {
			return this.color == color;
		}
		
		public int getColor() {
			return color;
		}
	}
	
	/**
	 * Builds a list of nodes which are square meshes that occupy the free areas of the map. Also builds the
	 * "cells" array that represents the full blown map and holds for each cell the node id to which it
	 * belongs and space for a minion's id.
	 * @param pixmapPath Path of raw bitmap path of map structure. Each pixel in this image represents a
	 * 10x10 game tile composed of 10x10 cells. The pixel's color determines if the tile is a wall or free.
	 */
	public void loadMap(String pixmapPath) {
		// load image file that represents level structure
		Pixmap pixmap = new Pixmap(Gdx.files.internal(pixmapPath));
		
		// create unprocessed map data
		int tileMapWidth = pixmap.getWidth();
		int tileMapHeight = pixmap.getHeight();
		int[][] tileMap = new int[tileMapWidth][tileMapHeight];
		
		/* Scan pixels from up to down, left to right. ATTENTION when reading a PNG with libgdx the image
		 * origin is the upper left corner while in libgdx the origin is in the lower left corner. So here
		 * the map image must flipped.
		 * Flag unoccupied pixels as empty */
		for (int y = 0; y < tileMapHeight; y++) {
			for (int x = 0; x < tileMapWidth; x++) {
				// get colour of current tile/pixel
				int currentPixel = pixmap.getPixel(x, tileMapHeight - y - 1);
				
				// identify tiles type
				if (PIXEL_TYPE.WALL.sameColor(currentPixel)) {	
					tileMap[x][y] = CELL_INVALID; // No minions or nodes can be placed here
				} else {
					tileMap[x][y] = CELL_FREE_NODE;
				}
			}
		}
		
		/*
		 * divide map into larger nodes. In each iteration get the biggest possible node. Then mark the map
		 * the tiles this node occupies so that the next search ommits these spaces. Finally the new node
		 * is added to the nodes list.
		 */
		PreNode nodeBiggest = new PreNode();
		PreNode nodeNext = new PreNode();
		nodes = new ArrayList<Node>(300);
		int nodeId = 1;
		boolean foundBiggerNode ;
		do {
			foundBiggerNode = false;
			nodeBiggest.size = 0;
			for (int y = 0; y < tileMapHeight; ++y) {
				for (int x = 0; x < tileMapWidth; ++x) {
					if (tileMap[x][y] == CELL_FREE_NODE) {
						nodeNext.init(x, y, nodeId);
						nodeNext.grow(tileMap, tileMapWidth, tileMapHeight);
						if (nodeNext.isBiggerThan(nodeBiggest)) {
							foundBiggerNode = true;
							nodeBiggest.copyFrom(nodeNext);
						}
					}
				}
			}
			if (foundBiggerNode) {
				nodeBiggest.markMap(tileMap); // mark occupied area in map so next search doesn't consider it
				Node node = new Node(nodeBiggest.drawX, nodeBiggest.drawY, nodeBiggest.drawWidth,
						nodeBiggest.drawHeight, nodeBiggest.id);
				nodes.add(node);
				nodeId++;
			}
		} while(foundBiggerNode);
		
		
		// fill in neighbours of each node
		// horizontal neighbours
		for (int y = 0; y < tileMapHeight; ++y) {
			for (int x = 0; x < tileMapWidth - 1; ++x) {
				if ((tileMap[x][y] != CELL_INVALID) && (tileMap[x + 1][y] != CELL_INVALID)
						&& (tileMap[x][y] != tileMap[x + 1][y])) {
					nodes.get(tileMap[x + 1][y] - 1).addNeighbour(nodes.get(tileMap[x][y] - 1));
					nodes.get(tileMap[x][y] - 1).addNeighbour(nodes.get(tileMap[x + 1][y] - 1));
				}
			}
		}
		// vertical neighbours
		for (int x = 0; x < tileMapWidth; ++x) {
			for (int y = 0; y < tileMapHeight - 1; ++y) {
				if ((tileMap[x][y] != CELL_INVALID) && (tileMap[x][y + 1] != CELL_INVALID)
						&& (tileMap[x][y] != tileMap[x][y + 1])) {
					nodes.get(tileMap[x][y] - 1).addNeighbour(nodes.get(tileMap[x][y + 1] - 1));
					nodes.get(tileMap[x][y + 1] - 1).addNeighbour(nodes.get(tileMap[x][y] - 1));
				}
			}
		}
		// diagonal neighbours
//		for (int x = 1; x < tileMapWidth; ++x) {
//			for (int y = 1; y < tileMapHeight - 1; ++y) {
//				// down left
//				if ((tileMap[x][y] != CELL_INVALID) && (tileMap[x - 1][y - 1] != CELL_INVALID)
//						&& (tileMap[x][y] != tileMap[x - 1][y - 1])) {
//					nodes.get(tileMap[x][y] - 1).addNeighbour(nodes.get(tileMap[x - 1][y - 1] - 1));
//					nodes.get(tileMap[x - 1][y - 1] - 1).addNeighbour(nodes.get(tileMap[x][y] - 1));
//				}
//				// down right
//				if ((tileMap[x][y] != CELL_INVALID) && (tileMap[x + 1][y - 1] != CELL_INVALID)
//						&& (tileMap[x][y] != tileMap[x + 1][y - 1])) {
//					nodes.get(tileMap[x][y] - 1).addNeighbour(nodes.get(tileMap[x + 1][y - 1] - 1));
//					nodes.get(tileMap[x + 1][y - 1] - 1).addNeighbour(nodes.get(tileMap[x][y] - 1));
//				}
//				// up left
//				if ((tileMap[x][y] != CELL_INVALID) && (tileMap[x - 1][y + 1] != CELL_INVALID)
//						&& (tileMap[x][y] != tileMap[x - 1][y - 1])) {
//					nodes.get(tileMap[x][y] - 1).addNeighbour(nodes.get(tileMap[x - 1][y + 1] - 1));
//					nodes.get(tileMap[x - 1][y + 1] - 1).addNeighbour(nodes.get(tileMap[x][y] - 1));
//				}
//				// up right
//				if ((tileMap[x][y] != CELL_INVALID) && (tileMap[x + 1][y + 1] != CELL_INVALID)
//						&& (tileMap[x][y] != tileMap[x + 1][y + 1])) {
//					nodes.get(tileMap[x][y] - 1).addNeighbour(nodes.get(tileMap[x + 1][y + 1] - 1));
//					nodes.get(tileMap[x + 1][y + 1] - 1).addNeighbour(nodes.get(tileMap[x][y] - 1));
//				}
//			}
//		}
		
		// create blowed up map data by marking the node id of each cell.
		width = 10 * tileMapWidth;
		height = 10 * tileMapHeight;
		cells = new int[width][height];
		for (int y = 0; y < tileMapHeight; y++) {
			for (int x = 0; x < tileMapWidth; x++) {
				for (int yy = 0; yy < 10; ++yy) {
					for (int xx = 0; xx < 10; ++xx) {
						cells[x * 10 + xx][y * 10 + yy] = tileMap[x][y];
					}
				}
			}
		}
	}
}
